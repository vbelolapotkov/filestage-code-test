const fs = require('fs');

const readFile = filePath => {
  return new Promise((resolve, reject) =>
    fs.readFile(filePath, 'utf8', (err, fileContent) => {
      if (err) {
        reject(err);
      } else {
        resolve(fileContent);
      }
    })
  );
};

module.exports = {
  readFile,
};
