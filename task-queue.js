module.exports = function makeTaskQueue({ concurrency = 1 } = {}) {
  const queueConcurrency = concurrency;
  let runningTasks = 0;
  const queue = [];

  return Object.freeze({
    pushTask,
  });

  function pushTask(task) {
    queue.push(task);
    executeNext();
  }

  function executeNext() {
    while (runningTasks < queueConcurrency && queue.length) {
      const task = queue.shift();
      task().then(() => {
        runningTasks--;
        executeNext();
      });
      runningTasks++;
    }
  }
};
