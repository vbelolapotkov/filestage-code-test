#Filestage code test: Most Valuable Player (MVP)

The app calculates MVP of a tournament.

Tournament consists of multiple matches in several disciplines.

The same players can compete in multiple disciplines. 
The MVP is a player with maximum rating across all matches and disciplines.

Each discipline has its own set of rules for calculating player rating.
Player rating for each match is calculated by processing stats file of the match.

The app supports adding new disciplines in the future.

All other requirements are described in a [docs/requirements.pdf](docs/requirements.pdf).

##Usage

Match stats files content can be passed to the app by multiple ways.
However, for demonstration purposes parallel file reader with limited concurrency is created.
The file reader reads specified files. Some sample files are created in `samples` folder.

Results of processing stats files are printed out in the console.

To run the app use `npm start`

To test the app use `npm test` or `npm run test:watch` 

##Assumptions
There may be multiple MVPs with equal rating points.

There may be no MVP at all.

##License
MIT

 

  


