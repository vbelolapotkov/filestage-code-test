module.exports = function makePlayersRating() {
  const ratings = new Map();
  let maxPoints;

  return Object.freeze({
    addPoints,
    getPoints,
    getMvp,
  });

  function addPoints(playerNickname, points) {
    const currentPoints = ratings.get(playerNickname);
    const newPoints =
      typeof currentPoints === 'number' ? currentPoints + points : points;
    ratings.set(playerNickname, newPoints);
    if (typeof maxPoints !== 'number' || newPoints > maxPoints) {
      maxPoints = newPoints;
    }
  }

  function getPoints(playerNickname) {
    const points = ratings.get(playerNickname);
    return typeof points === 'number' ? points : 0;
  }

  function getMvp() {
    const mvps = [];
    ratings.forEach((points, nickname) => {
      if (points === maxPoints) {
        mvps.push({ nickname, points });
      }
    });
    return mvps;
  }
};
