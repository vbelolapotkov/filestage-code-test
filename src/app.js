const makePlayersRating = require('./players-rating');
const makeMatchFilesProcessor = require('./match-files-processor');

module.exports = function makeMvpApp(initialPlayersRating) {
  const playersRating = initialPlayersRating || makePlayersRating();
  const matchFilesProcessor = makeMatchFilesProcessor(playersRating);

  return Object.freeze({
    processMatchFile,
    getMvp,
  });

  function processMatchFile(matchFileContent) {
    matchFilesProcessor.processMatchFile(matchFileContent);
  }

  function getMvp() {
    return playersRating.getMvp();
  }
};
