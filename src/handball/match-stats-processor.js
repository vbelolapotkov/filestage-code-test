const makeMatchStatsProcessor = require('../match-stats-processor');
const parseHandballPalyerStats = require('./player-stats-parser');
const makeHandballMatchStatsCalculator = require('./match-stats-calculator');

const matchType = 'HANDBALL';
const makeHandballMatchStatsProcessor = playerRatings =>
  makeMatchStatsProcessor(
    matchType,
    parseHandballPalyerStats,
    makeHandballMatchStatsCalculator(playerRatings)
  );
makeHandballMatchStatsProcessor.matchType = matchType;

module.exports = makeHandballMatchStatsProcessor;
