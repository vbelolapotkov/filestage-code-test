module.exports = {
  matchWonPoints: 10,
  calcStatPoints,
};

function calcStatPoints({ position, goalsMade = 0, goalsReceived = 0 }) {
  switch (position) {
    case 'G':
      return calcGoalkeeperRatingPoints(goalsMade, goalsReceived);
    case 'F':
      return calcFieldPlayerRatingPoints(goalsMade, goalsReceived);
    default:
      throw new Error('UNKNOWN_PLAYER_POSITION');
  }
}

function calcGoalkeeperRatingPoints(goalsMade, goalsReceived) {
  return 50 + 5 * goalsMade - 2 * goalsReceived;
}

function calcFieldPlayerRatingPoints(goalsMade, goalsReceived) {
  return 20 + goalsMade - goalsReceived;
}
