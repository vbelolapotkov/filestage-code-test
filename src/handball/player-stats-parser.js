const STATS_DELIMITER = ';';

module.exports = function parsePlayerStats(playerStatsStr) {
  try {
    const [
      ,
      nickname,
      ,
      team,
      position,
      goalsMade,
      goalsReceived,
    ] = playerStatsStr.split(STATS_DELIMITER);

    return {
      nickname,
      position,
      team,
      goalsMade: parseNumber(goalsMade),
      goalsReceived: parseNumber(goalsReceived),
    };
  } catch (err) {
    const formatError = new Error('BAD_FORMAT');
    formatError.details = err.message;
    throw formatError;
  }
};

function parseNumber(rawValue) {
  const value = Number(rawValue);
  if (Number.isNaN(value)) {
    throw new Error('NUMBER_EXPECTED');
  }

  return value;
}
