const makePlayersRating = require('../../players-rating');
const makeHandballMatchStatsProcessor = require('../match-stats-processor');
const playerStatsCalculator = require('../player-stats-calculator');

describe('handball match stats processor', () => {
  let playersRating;
  let statsProcessor;

  beforeEach(() => {
    playersRating = makePlayersRating();
    statsProcessor = makeHandballMatchStatsProcessor(playersRating);
  });

  test('adds goalkeeper points', () => {
    const stats = [
      'player 1;nick1;4;Team A;G;0;1',
      'player 2;nick2;16;Team B;G;1;0',
    ];

    statsProcessor.processStats(stats);

    const nick1ExpectedPoints = playerStatsCalculator.calcStatPoints({
      position: 'G',
      goalsReceived: 1,
    });
    const nick2ExpectedPoints =
      playerStatsCalculator.calcStatPoints({
        position: 'G',
        goalsMade: 1,
        goalsReceived: 0,
      }) + playerStatsCalculator.matchWonPoints;

    const nick1Points = playersRating.getPoints('nick1');
    expect(nick1Points).toEqual(nick1ExpectedPoints);
    const nick2Points = playersRating.getPoints('nick2');
    expect(nick2Points).toEqual(nick2ExpectedPoints);
  });

  test('adds field player points', () => {
    const stats = [
      'player 1;nick1;8;Team A;F;15;12',
      'player 2;nick2;23;Team B;F;12;15',
    ];

    statsProcessor.processStats(stats);
    const nick1ExpectedPoints =
      playerStatsCalculator.calcStatPoints({
        position: 'F',
        goalsMade: 15,
        goalsReceived: 12,
      }) + playerStatsCalculator.matchWonPoints;
    const nick2ExpectedPoints = playerStatsCalculator.calcStatPoints({
      position: 'F',
      goalsMade: 12,
      goalsReceived: 15,
    });

    const nick1Points = playersRating.getPoints('nick1');
    expect(nick1Points).toEqual(nick1ExpectedPoints);
    const nick2Points = playersRating.getPoints('nick2');
    expect(nick2Points).toEqual(nick2ExpectedPoints);
  });

  test('throws if stat format is wrong', () => {
    const stats = [
      'player 1;nick1;23;Team B;F;4;7',
      'player 2;nick2;8;Team A;F;7;B',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('BAD_FORMAT');
  });

  test('throws if no second team', () => {
    const stats = [
      'player 1;nick1;23;Team A;G;0;7',
      'player 2;nick2;8;Team A;F;5;7',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('SINGLE_TEAM');
  });

  test('throws if third team detected', () => {
    const stats = [
      'player 1;nick1;23;Team B;F;4;7',
      'player 2;nick2;8;Team A;F;0;10',
      'player 2;nick3;8;Team C;F;10;8',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('TOO_MANY_TEAMS');
  });

  test('throws if no winning team', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;3;7',
      'player 2;nick2;22;Team A;F;4;7',
      'player 3;nick3;8;Team B;F;2;7',
      'player 4;nick4;9;Team B;F;5;7',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('NO_WINNING_TEAM');
  });

  test('throws if same player detected in different teams', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;10;7',
      'player 2;nick1;24;Team B;F;7;10',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow(
      'SAME_PLAYER_IN_DIFFERENT_TEAMS'
    );
  });

  test('throws if made and received goals mismatch', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;3;7',
      'player 2;nick2;22;Team A;F;4;7',
      'player 3;nick3;8;Team B;F;4;7',
      'player 4;nick4;9;Team B;F;4;7',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow(
      'MADE_RECEIVED_GOALS_MISMATCH'
    );
  });

  test('throws if team players have different amount of goals received', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;3;7',
      'player 2;nick2;22;Team A;F;4;8',
      'player 3;nick3;8;Team B;F;4;7',
      'player 4;nick4;9;Team B;F;4;7',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow(
      'TEAM_RECEIVED_GOALS_MISMATCH'
    );
  });
});
