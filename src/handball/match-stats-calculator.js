const handballStatsCalculator = require('./player-stats-calculator');

module.exports = function makeMatchStatsCalculator(playersRating) {
  const teamsStats = new Map();

  return Object.freeze({
    processPlayerStats,
    addPointsToWinningTeam,
  });

  function processPlayerStats(playerStats) {
    const { nickname, team, position, goalsMade, goalsReceived } = playerStats;
    const ratingPoints = handballStatsCalculator.calcStatPoints({
      position,
      goalsMade,
      goalsReceived,
    });

    addGoalsMadeToTeam({ team, nickname, goalsMade, goalsReceived });
    validatePlayer({ nickname, team, goalsReceived });
    playersRating.addPoints(nickname, ratingPoints);
  }

  function validatePlayer({ nickname, team, goalsReceived }) {
    teamsStats.forEach((teamStats, teamName) => {
      if (teamName !== team && teamStats.roaster.has(nickname)) {
        throw new Error('SAME_PLAYER_IN_DIFFERENT_TEAMS');
      }

      if (teamName === team && teamStats.goalsReceived !== goalsReceived) {
        throw new Error('TEAM_RECEIVED_GOALS_MISMATCH');
      }
    });
  }

  function addGoalsMadeToTeam({ team, nickname, goalsMade, goalsReceived }) {
    if (!teamsStats.has(team)) {
      initTeamStats(team, goalsReceived);
    }

    const teamStats = teamsStats.get(team);
    teamStats.goalsMade += goalsMade;
    teamStats.roaster.add(nickname);
    teamsStats.set(team, teamStats);
  }

  function initTeamStats(team, goalsReceived = 0) {
    if (teamsStats.size > 1) {
      throw new Error('TOO_MANY_TEAMS');
    }
    teamsStats.set(team, {
      goalsMade: 0,
      goalsReceived,
      roaster: new Set(),
    });
  }

  function addPointsToWinningTeam() {
    const winningTeamStats = getWinningTeamStats();
    winningTeamStats.roaster.forEach(nickname =>
      playersRating.addPoints(nickname, handballStatsCalculator.matchWonPoints)
    );
  }

  function getWinningTeamStats() {
    validateTeamsStats();
    let maxGoalsMade = 0;
    let winningTeamStats;

    teamsStats.forEach(teamStats => {
      if (teamStats.goalsMade > maxGoalsMade) {
        maxGoalsMade = teamStats.goalsMade;
        winningTeamStats = teamStats;
      }
    });

    return winningTeamStats;
  }

  function validateTeamsStats() {
    if (teamsStats.size === 1) {
      throw new Error('SINGLE_TEAM');
    }

    const [team1Stats, team2Stats] = teamsStats.values();

    if (
      !team1Stats ||
      !team2Stats ||
      team1Stats.goalsMade === team2Stats.goalsMade
    ) {
      throw new Error('NO_WINNING_TEAM');
    }

    if (
      team1Stats.goalsMade !== team2Stats.goalsReceived ||
      team1Stats.goalsReceived !== team2Stats.goalsMade
    ) {
      throw new Error('MADE_RECEIVED_GOALS_MISMATCH');
    }
  }
};
