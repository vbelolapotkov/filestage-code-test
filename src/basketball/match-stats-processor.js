const makeMatchStatsProcessor = require('../match-stats-processor');
const makeBasketballMatchStatsCalculator = require('./match-stats-calculator');
const parseBasketballPlayerStats = require('./player-stats-parser');

const matchType = 'BASKETBALL';
const makeBasketballMatchStatsProcessor = playersRating =>
  makeMatchStatsProcessor(
    matchType,
    parseBasketballPlayerStats,
    makeBasketballMatchStatsCalculator(playersRating)
  );
makeBasketballMatchStatsProcessor.matchType = matchType;

module.exports = makeBasketballMatchStatsProcessor;
