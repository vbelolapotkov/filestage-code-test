const basketballStatsCalculator = require('./player-stats-calculator');

module.exports = function makeMatchStatsCalculator(playersRating) {
  const teamsStats = new Map();

  return Object.freeze({
    processPlayerStats,
    addPointsToWinningTeam,
  });

  function processPlayerStats(playerStats) {
    const {
      nickname,
      position,
      team,
      scoredPoints,
      rebounds,
      assists,
    } = playerStats;

    const ratingPoints = basketballStatsCalculator.calcStatPoints({
      position,
      scoredPoints,
      rebounds,
      assists,
    });

    validatePlayer(nickname, team);
    playersRating.addPoints(nickname, ratingPoints);
    addScoredPointsToTeam({ team, nickname, scoredPoints });
  }

  function validatePlayer(nickname, team) {
    teamsStats.forEach(({ roaster }, teamName) => {
      if (teamName !== team && roaster.has(nickname)) {
        throw new Error('SAME_PLAYER_IN_DIFFERENT_TEAMS');
      }
    });
  }

  function addScoredPointsToTeam({ team, nickname, scoredPoints }) {
    if (!teamsStats.has(team)) {
      initTeamStats(team);
    }

    const teamStats = teamsStats.get(team);
    teamStats.scoredPoints += scoredPoints;
    teamStats.roaster.add(nickname);
    teamsStats.set(team, teamStats);
  }

  function initTeamStats(team) {
    if (teamsStats.size > 1) {
      throw new Error('TOO_MANY_TEAMS');
    }
    teamsStats.set(team, { scoredPoints: 0, roaster: new Set() });
  }

  function addPointsToWinningTeam() {
    const winningTeamStats = getWinningTeamStats();
    winningTeamStats.roaster.forEach(nickname =>
      playersRating.addPoints(
        nickname,
        basketballStatsCalculator.matchWonPoints
      )
    );
  }

  function getWinningTeamStats() {
    validateTeamsStats();
    let maxScore = 0;
    let winningTeamStats;

    teamsStats.forEach(teamStats => {
      if (teamStats.scoredPoints > maxScore) {
        maxScore = teamStats.scoredPoints;
        winningTeamStats = teamStats;
      }
    });

    return winningTeamStats;
  }

  function validateTeamsStats() {
    if (teamsStats.size === 1) {
      throw new Error('SINGLE_TEAM');
    }

    const [team1Stats, team2Stats] = teamsStats.values();
    if (
      !team1Stats ||
      !team2Stats ||
      team1Stats.scoredPoints === team2Stats.scoredPoints
    ) {
      throw new Error('NO_WINNING_TEAM');
    }
  }
};
