module.exports = {
  matchWonPoints: 10,
  calcStatPoints,
};

function calcStatPoints({
  position,
  scoredPoints = 0,
  rebounds = 0,
  assists = 0,
}) {
  switch (position) {
    case 'G':
      return calcGuardRatingPoints(scoredPoints, rebounds, assists);
    case 'F':
      return calcForwardRatingPoints(scoredPoints, rebounds, assists);
    case 'C':
      return calcCenterRatingPoints(scoredPoints, rebounds, assists);
    default:
      throw new Error('UNKNOWN_PLAYER_POSITION');
  }
}

function calcGuardRatingPoints(scoredPoints, rebounds, assists) {
  return 2 * scoredPoints + 3 * rebounds + assists;
}

function calcForwardRatingPoints(scoredPoints, rebounds, assists) {
  return 2 * scoredPoints + 2 * rebounds + 2 * assists;
}

function calcCenterRatingPoints(scoredPoints, rebounds, assists) {
  return 2 * scoredPoints + rebounds + 3 * assists;
}
