const makePlayersRating = require('../../players-rating');
const makeBasketballMatchStatsProcessor = require('../match-stats-processor');
const playerStatsCalculator = require('../player-stats-calculator');

describe('basketball match stats processor', () => {
  let playersRating;
  let statsProcessor;

  beforeEach(() => {
    playersRating = makePlayersRating();
    statsProcessor = makeBasketballMatchStatsProcessor(playersRating);
  });

  test('adds guard rating points', () => {
    const stats = [
      'player 1;nick1;4;Team A;G;10;2;7',
      'player 2;nick2;16;Team B;G;20;0;0',
    ];

    statsProcessor.processStats(stats);
    const nick1ExpectedPoints = playerStatsCalculator.calcStatPoints({
      position: 'G',
      scoredPoints: 10,
      rebounds: 2,
      assists: 7,
    });
    const nick2ExpectedPoints =
      playerStatsCalculator.calcStatPoints({
        position: 'G',
        scoredPoints: 20,
      }) + playerStatsCalculator.matchWonPoints;

    const nick1Points = playersRating.getPoints('nick1');
    expect(nick1Points).toEqual(nick1ExpectedPoints);
    const nick2Points = playersRating.getPoints('nick2');
    expect(nick2Points).toEqual(nick2ExpectedPoints);
  });

  test('adds forward rating points', () => {
    const stats = [
      'player 1;nick1;23;Team B;F;4;7;7',
      'player 2;nick2;8;Team A;F;0;10;0',
    ];

    statsProcessor.processStats(stats);
    const nick1ExpectedPoints =
      playerStatsCalculator.calcStatPoints({
        position: 'F',
        scoredPoints: 4,
        rebounds: 7,
        assists: 7,
      }) + playerStatsCalculator.matchWonPoints;
    const nick2ExpectedPoints = playerStatsCalculator.calcStatPoints({
      position: 'F',
      rebounds: 10,
    });

    const nick1Points = playersRating.getPoints('nick1');
    expect(nick1Points).toEqual(nick1ExpectedPoints);
    const nick2Points = playersRating.getPoints('nick2');
    expect(nick2Points).toEqual(nick2ExpectedPoints);
  });

  test('adds center rating points', () => {
    const stats = [
      'player 1;nick1;15;Team A;C;15;10;4',
      'player 2;nick2;42;Team B;C;8;10;0',
    ];

    statsProcessor.processStats(stats);
    const nick1ExpectedPoints =
      playerStatsCalculator.calcStatPoints({
        position: 'C',
        scoredPoints: 15,
        rebounds: 10,
        assists: 4,
      }) + playerStatsCalculator.matchWonPoints;

    const nick2ExpectedPoints = playerStatsCalculator.calcStatPoints({
      position: 'C',
      scoredPoints: 8,
      rebounds: 10,
    });

    const nick1Points = playersRating.getPoints('nick1');
    expect(nick1Points).toEqual(nick1ExpectedPoints);
    const nick2Points = playersRating.getPoints('nick2');
    expect(nick2Points).toEqual(nick2ExpectedPoints);
  });

  test('throws if stat format is wrong', () => {
    const stats = [
      'player 1;nick1;23;Team B;F;4;7;7',
      'player 2;nick2;8;Team A;F;0;B;0',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('BAD_FORMAT');
  });

  test('throws if no second team', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;4;7;7',
      'player 2;nick2;8;Team A;F;0;10;0',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('SINGLE_TEAM');
  });

  test('throws if third team detected', () => {
    const stats = [
      'player 1;nick1;23;Team B;F;4;7;7',
      'player 2;nick2;8;Team A;F;0;10;0',
      'player 2;nick3;8;Team C;F;10;8;0',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('TOO_MANY_TEAMS');
  });

  test('throws if no winning team', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;10;7;7',
      'player 2;nick2;22;Team A;F;10;7;7',
      'player 3;nick3;8;Team B;F;5;10;0',
      'player 4;nick4;9;Team B;F;15;1;0',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow('NO_WINNING_TEAM');
  });

  test('throws if the same player detected in different teams', () => {
    const stats = [
      'player 1;nick1;23;Team A;F;10;7;7',
      'player 2;nick1;24;Team B;F;10;7;7',
    ];

    expect(() => statsProcessor.processStats(stats)).toThrow(
      'SAME_PLAYER_IN_DIFFERENT_TEAMS'
    );
  });
});
