const STATS_DELIMITER = ';';

module.exports = function parsePlayerStats(playerStatsStr) {
  try {
    const [
      ,
      nickname,
      ,
      team,
      position,
      scoredPoints,
      rebounds,
      assists,
    ] = playerStatsStr.split(STATS_DELIMITER);

    return {
      nickname,
      position,
      team,
      scoredPoints: parseNumber(scoredPoints),
      rebounds: parseNumber(rebounds),
      assists: parseNumber(assists),
    };
  } catch (err) {
    const formatError = new Error('BAD_FORMAT');
    formatError.details = err.message;
    throw formatError;
  }
};

function parseNumber(rawValue) {
  const value = Number(rawValue);
  if (Number.isNaN(value)) {
    throw new Error('NUMBER_EXPECTED');
  }

  return value;
}
