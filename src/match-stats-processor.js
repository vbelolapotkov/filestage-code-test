const makeMatchStatsProcessor = (
  matchType,
  parsePlayerStats,
  matchStatsCalculator
) => {
  return Object.freeze({
    matchType,
    processStats,
  });

  function processStats(matchStats) {
    try {
      matchStats.forEach(processPlayerStats);
      matchStatsCalculator.addPointsToWinningTeam();
    } catch (err) {
      throw new Error(
        `FAILED_TO_PROCESS_MATCH_STATS: ${matchType}: ${err.message}`
      );
    }
  }

  function processPlayerStats(playerStatsStr) {
    try {
      if (!playerStatsStr) {
        return;
      }
      const playerStats = parsePlayerStats(playerStatsStr);
      matchStatsCalculator.processPlayerStats(playerStats);
    } catch (err) {
      throw new Error(
        `FAILED_TO_PROCESS_PLAYER_STATS: ${err.message}: ${playerStatsStr}`
      );
    }
  }
};

module.exports = makeMatchStatsProcessor;
