const makeBasketballStatsProcessor = require('./basketball/match-stats-processor');
const makeHandballStatsProcessor = require('./handball/match-stats-processor');

module.exports = function makeMatchFilesProcessor(playersRating) {
  const knownProcessors = new Map();
  initMatchFileProcessors();

  return Object.freeze({
    processMatchFile,
  });

  function processMatchFile(matchFileContent) {
    const lines = matchFileContent.split('\n');
    const matchType = lines
      .shift()
      .trim()
      .toUpperCase();
    if (!isKnownMatchType(matchType)) {
      throwUnknownMatchType(matchType);
    }

    const processor = knownProcessors.get(matchType)(playersRating);
    processor.processStats(lines);
  }

  function initMatchFileProcessors() {
    const processorFactories = [
      makeBasketballStatsProcessor,
      makeHandballStatsProcessor,
    ];

    processorFactories.forEach(matchStatsProcessorFactory =>
      knownProcessors.set(
        matchStatsProcessorFactory.matchType,
        matchStatsProcessorFactory
      )
    );
  }

  function isKnownMatchType(matchType) {
    return knownProcessors.has(matchType);
  }

  function throwUnknownMatchType(matchType) {
    const errType = 'UNKNOWN_MATCH_TYPE';
    const err = new Error(errType);
    err.details = { matchType };
    err.type = 'UNKNOWN_MATCH_TYPE';
    throw err;
  }
};
