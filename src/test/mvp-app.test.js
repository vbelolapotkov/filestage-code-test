const fs = require('fs');
const path = require('path');
const makeMvpApp = require('../app');

describe('mvp app', () => {
  test('can process basketball file', () => {
    const app = makeMvpApp();
    const filePath = path.join(__dirname, '../../samples/bb.csv');
    const matchFile = fs.readFileSync(filePath, 'utf8');
    app.processMatchFile(matchFile);
  });
  test('can process handball file', () => {
    const app = makeMvpApp();
    const filePath = path.join(__dirname, '../../samples/hb.csv');
    const matchFile = fs.readFileSync(filePath, 'utf8');
    app.processMatchFile(matchFile);
  });
  test('cannot process unknown file', () => {
    const app = makeMvpApp();
    const filePath = path.join(__dirname, '../../samples/unknown.csv');
    const matchFile = fs.readFileSync(filePath, 'utf8');
    expect(() => app.processMatchFile(matchFile)).toThrow('UNKNOWN_MATCH_TYPE');
  });
});
