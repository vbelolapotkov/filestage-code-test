const makePlayersRating = require('../players-rating');

describe('players rating', () => {
  let playersRating;
  const player1Nickname = 'nickname1';
  const player2Nickname = 'nickname2';

  beforeEach(() => {
    playersRating = makePlayersRating();
  });

  test('adds new player to rating', () => {
    playersRating.addPoints(player1Nickname, 10);
    const points = playersRating.getPoints(player1Nickname);
    expect(points).toEqual(10);
  });

  test('returns default rating for unknown player', () => {
    const points = playersRating.getPoints('nickname999');
    expect(points).toEqual(0);
  });

  test('updates player rating', () => {
    playersRating.addPoints(player1Nickname, 5);
    playersRating.addPoints(player1Nickname, 6);
    const points = playersRating.getPoints(player1Nickname);
    expect(points).toEqual(5 + 6);
  });

  test('returns mvp', () => {
    playersRating.addPoints(player1Nickname, 5);
    playersRating.addPoints(player2Nickname, 4);
    let mvp = playersRating.getMvp();
    expect(mvp).toEqual([{ nickname: player1Nickname, points: 5 }]);
  });

  test('updates mvp', () => {
    playersRating.addPoints(player1Nickname, 5);
    let mvp = playersRating.getMvp();
    expect(mvp).toEqual([{ nickname: player1Nickname, points: 5 }]);

    playersRating.addPoints(player2Nickname, 6);
    mvp = playersRating.getMvp();
    expect(mvp).toEqual([{ nickname: player2Nickname, points: 6 }]);
  });

  test('returns multiple mvps in case of tie', () => {
    const player3Nickname = 'nickname3';

    playersRating.addPoints(player1Nickname, 5);
    playersRating.addPoints(player2Nickname, 4);
    playersRating.addPoints(player3Nickname, 5);

    const mvp = playersRating.getMvp();
    expect(mvp).toEqual([
      { nickname: player1Nickname, points: 5 },
      { nickname: player3Nickname, points: 5 },
    ]);
  });
});
