const path = require('path');
const makeMvpApp = require('./src/app');
const makeFileReader = require('./file-reader');

const sampleFiles = [
  path.join(__dirname, 'samples/bb.csv'),
  path.join(__dirname, 'samples/hb.csv'),
  path.join(__dirname, 'samples/bb2.csv'),
  path.join(__dirname, 'samples/hb2.csv'),
];
const readerConfig = {
  concurrency: 5
}

runApp(makeFileReader(readerConfig), makeMvpApp());

async function runApp(fileReader, app) {
  try {
    await fileReader.readFiles(sampleFiles, app.processMatchFile);
    const mvp = app.getMvp();
    printMvp(mvp);
  } catch (err) {
    console.error(`Failed to calculate MVP: ${err.message}`);
  }
}

function printMvp(mvp) {
  if (!mvp || mvp.length === 0) {
    console.log(':( No MVPs found');
    return;
  }

  if (mvp.length === 1) {
    const { nickname, points } = mvp[0];
    console.log(`MVP is ${nickname} with rating ${points}`);
  } else {
    console.log(`We have a tie between ${mvp.length} MVPs`);
    mvp.forEach(({ nickname, points }, index) =>
      console.log(`${index + 1}. ${nickname}, ${points} points`)
    );
  }
}
