const { readFile } = require('./utils/file');
const makeTaskQueue = require('./task-queue');

module.exports = function makeFileReader({ concurrency } = {}) {
  const readQueue = makeTaskQueue({ concurrency });
  return Object.freeze({
    readFiles,
  });

  async function readFiles(pathList = [], processFile) {
    return new Promise((resolve, reject) => {
      const filesCount = pathList.length;
      let completed = 0;
      let errored = false;

      pathList.forEach(filePath => {
        const task = async () => {
          try {
            if (errored) {
              // already failed no need to read next file
              return;
            }

            const fileContent = await readFile(filePath);
            await processFile(filePath, fileContent);

            if (++completed === filesCount) {
              resolve();
            }
          } catch (err) {
            if (!errored) {
              errored = true;
              reject(err);
            }
          }
        };
        readQueue.pushTask(task);
      });
    });
  }
};
